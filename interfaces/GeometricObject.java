package interfaces;

public interface GeometricObject {
    public double getPerimeter();

    public double getArea();
}
