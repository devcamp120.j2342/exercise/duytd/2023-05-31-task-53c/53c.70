import models.Circle;
import models.ResizableCircle;

public class App {
    public static void main(String[] args) throws Exception {
        
        Circle circle = new Circle(15);

        ResizableCircle resizable = new ResizableCircle(18);

        System.out.println(circle);
        System.out.println(resizable);

        System.out.println("Chu vi" + " " + circle.getPerimeter());
        System.out.println("Ban kinh" + " " + circle.getArea());

        System.out.println("Chu vi" + " " + resizable.getPerimeter());
        System.out.println("Dien tich" + " " + resizable.getArea());
        resizable.resize(20);
        System.out.println("thay doi kich thuoc" + " " + resizable);

        

    }
}
